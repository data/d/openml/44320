{
  "data_set_description": {
    "collection_date": "30-09-2022",
    "creator": "Ihsan Ullah",
    "default_target_attribute": "CATEGORY",
    "description": "## **Meta-Album Birds Dataset (Extended)**\n***\nWhen Meta-Album was created, the Birds dataset(https://www.kaggle.com/datasets/gpiosenka/100-bird-species) contained images of 315 bird species, but now it has increased the number of species to 450. It has more than 49 000 images, each with a resolution of 224x224 px. All the images have their natural background, which can lead to bias since, for example, some birds are frequently found in water backgrounds. Additionally, the dataset is imbalanced regarding the ratio of male species images to female species images. The preprocessed version distributed in Meta-Album is made from the original dataset by resizing all the images to a resolution of 128x128 px using an anti-aliasing filter.  \n\n\n\n### **Dataset Details**\n![](https://meta-album.github.io/assets/img/samples/BRD.png)\n\n**Meta Album ID**: LR_AM.BRD  \n**Meta Album URL**: [https://meta-album.github.io/datasets/BRD.html](https://meta-album.github.io/datasets/BRD.html)  \n**Domain ID**: LR_AM  \n**Domain Name**: Large Animals  \n**Dataset ID**: BRD  \n**Dataset Name**: Birds  \n**Short Description**: Birds dataset for image classification  \n**\\# Classes**: 315  \n**\\# Images**: 49054  \n**Keywords**: birds, animals  \n**Data Format**: images  \n**Image size**: 128x128  \n\n**License (original data release)**: CC0 Public Domain  \n**License URL(original data release)**: https://www.kaggle.com/gpiosenka/100-bird-species\nhttps://creativecommons.org/publicdomain/zero/1.0/\n \n**License (Meta-Album data release)**: CC0 Public Domain  \n**License URL (Meta-Album data release)**: [https://creativecommons.org/publicdomain/zero/1.0/](https://creativecommons.org/publicdomain/zero/1.0/)  \n\n**Source**: BIRDS 400 - SPECIES IMAGE CLASSIFICATION  \n**Source URL**: https://www.kaggle.com/gpiosenka/100-bird-species  \n  \n**Original Author**: Gerald Piosenka  \n**Original contact**: https://www.kaggle.com/gpiosenka/contact  \n\n**Meta Album author**: Dustin Carrion  \n**Created Date**: 01 March 2022  \n**Contact Name**: Ihsan Ullah  \n**Contact Email**: meta-album@chalearn.org  \n**Contact URL**: [https://meta-album.github.io/](https://meta-album.github.io/)  \n\n\n\n### **Cite this dataset**\n```\n@article{birds,\n    title={BIRDS 400 - SPECIES IMAGE CLASSIFICATION},\n    author={Gerald Piosenka},\n    url={https://www.kaggle.com/datasets/gpiosenka/100-bird-species},\n    publisher= {Kaggle}\n}\n```\n\n\n### **Cite Meta-Album**\n```\n@inproceedings{meta-album-2022,\n        title={Meta-Album: Multi-domain Meta-Dataset for Few-Shot Image Classification},\n        author={Ullah, Ihsan and Carrion, Dustin and Escalera, Sergio and Guyon, Isabelle M and Huisman, Mike and Mohr, Felix and van Rijn, Jan N and Sun, Haozhe and Vanschoren, Joaquin and Vu, Phan Anh},\n        booktitle={Thirty-sixth Conference on Neural Information Processing Systems Datasets and Benchmarks Track},\n        url = {https://meta-album.github.io/},\n        year = {2022}\n    }\n```\n\n\n### **More**\nFor more information on the Meta-Album dataset, please see the [[NeurIPS 2022 paper]](https://meta-album.github.io/paper/Meta-Album.pdf)  \nFor details on the dataset preprocessing, please see the [[supplementary materials]](https://openreview.net/attachment?id=70_Wx-dON3q&name=supplementary_material)  \nSupporting code can be found on our [[GitHub repo]](https://github.com/ihsaan-ullah/meta-album)  \nMeta-Album on Papers with Code [[Meta-Album]](https://paperswithcode.com/dataset/meta-album)  \n\n\n\n### **Other versions of this dataset**\n[[Micro]](https://www.openml.org/d/44241)  [[Mini]](https://www.openml.org/d/44285)",
    "description_version": "3",
    "file_id": "22111034",
    "format": "arff",
    "id": "44320",
    "language": "English",
    "licence": "CC BY-NC 4.0",
    "md5_checksum": "8945251eedaa56d15793754896aa95e0",
    "minio_url": "https://data.openml.org/datasets/0004/44320/dataset_44320.pq",
    "name": "Meta_Album_BRD_Extended",
    "parquet_url": "https://data.openml.org/datasets/0004/44320/dataset_44320.pq",
    "processing_date": "2022-11-08 17:30:03",
    "status": "active",
    "upload_date": "2022-11-08T17:29:58",
    "url": "https://api.openml.org/data/v1/download/22111034/Meta_Album_BRD_Extended.arff",
    "version": "1",
    "visibility": "public"
  }
}