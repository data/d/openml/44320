# OpenML dataset: Meta_Album_BRD_Extended

https://www.openml.org/d/44320

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

## **Meta-Album Birds Dataset (Extended)**
***
When Meta-Album was created, the Birds dataset(https://www.kaggle.com/datasets/gpiosenka/100-bird-species) contained images of 315 bird species, but now it has increased the number of species to 450. It has more than 49 000 images, each with a resolution of 224x224 px. All the images have their natural background, which can lead to bias since, for example, some birds are frequently found in water backgrounds. Additionally, the dataset is imbalanced regarding the ratio of male species images to female species images. The preprocessed version distributed in Meta-Album is made from the original dataset by resizing all the images to a resolution of 128x128 px using an anti-aliasing filter.  



### **Dataset Details**
![](https://meta-album.github.io/assets/img/samples/BRD.png)

**Meta Album ID**: LR_AM.BRD  
**Meta Album URL**: [https://meta-album.github.io/datasets/BRD.html](https://meta-album.github.io/datasets/BRD.html)  
**Domain ID**: LR_AM  
**Domain Name**: Large Animals  
**Dataset ID**: BRD  
**Dataset Name**: Birds  
**Short Description**: Birds dataset for image classification  
**\# Classes**: 315  
**\# Images**: 49054  
**Keywords**: birds, animals  
**Data Format**: images  
**Image size**: 128x128  

**License (original data release)**: CC0 Public Domain  
**License URL(original data release)**: https://www.kaggle.com/gpiosenka/100-bird-species
https://creativecommons.org/publicdomain/zero/1.0/
 
**License (Meta-Album data release)**: CC0 Public Domain  
**License URL (Meta-Album data release)**: [https://creativecommons.org/publicdomain/zero/1.0/](https://creativecommons.org/publicdomain/zero/1.0/)  

**Source**: BIRDS 400 - SPECIES IMAGE CLASSIFICATION  
**Source URL**: https://www.kaggle.com/gpiosenka/100-bird-species  
  
**Original Author**: Gerald Piosenka  
**Original contact**: https://www.kaggle.com/gpiosenka/contact  

**Meta Album author**: Dustin Carrion  
**Created Date**: 01 March 2022  
**Contact Name**: Ihsan Ullah  
**Contact Email**: meta-album@chalearn.org  
**Contact URL**: [https://meta-album.github.io/](https://meta-album.github.io/)  



### **Cite this dataset**
```
@article{birds,
    title={BIRDS 400 - SPECIES IMAGE CLASSIFICATION},
    author={Gerald Piosenka},
    url={https://www.kaggle.com/datasets/gpiosenka/100-bird-species},
    publisher= {Kaggle}
}
```


### **Cite Meta-Album**
```
@inproceedings{meta-album-2022,
        title={Meta-Album: Multi-domain Meta-Dataset for Few-Shot Image Classification},
        author={Ullah, Ihsan and Carrion, Dustin and Escalera, Sergio and Guyon, Isabelle M and Huisman, Mike and Mohr, Felix and van Rijn, Jan N and Sun, Haozhe and Vanschoren, Joaquin and Vu, Phan Anh},
        booktitle={Thirty-sixth Conference on Neural Information Processing Systems Datasets and Benchmarks Track},
        url = {https://meta-album.github.io/},
        year = {2022}
    }
```


### **More**
For more information on the Meta-Album dataset, please see the [[NeurIPS 2022 paper]](https://meta-album.github.io/paper/Meta-Album.pdf)  
For details on the dataset preprocessing, please see the [[supplementary materials]](https://openreview.net/attachment?id=70_Wx-dON3q&name=supplementary_material)  
Supporting code can be found on our [[GitHub repo]](https://github.com/ihsaan-ullah/meta-album)  
Meta-Album on Papers with Code [[Meta-Album]](https://paperswithcode.com/dataset/meta-album)  



### **Other versions of this dataset**
[[Micro]](https://www.openml.org/d/44241)  [[Mini]](https://www.openml.org/d/44285)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44320) of an [OpenML dataset](https://www.openml.org/d/44320). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44320/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44320/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44320/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

